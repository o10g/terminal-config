#!/bin/bash

#VAGRANT_CWD=/Users/dsavchenko/Projects/devenv

# First you need to update your my.cnf to enable the general log in config.
# After the change in my.cnf it will be possible to use this script

# [mysqld]
# general_log_file        = /var/log/mysql/mysql.log
# general_log             = 0


cd $DEVENV_PATH
vagrant ssh --command 'mysql -e "SET GLOBAL general_log = 'ON';"' -- -q
echo "Mysql says:"
vagrant ssh --command "sudo tail -n0 -f /var/log/mysql/mysql.log" -- -q
echo ""
echo -n "Cleaning up... "
vagrant ssh --command 'mysql -e "SET GLOBAL general_log = 'OFF';"' -- -q
cd - 2>&1 /dev/null
echo "Bye!"
