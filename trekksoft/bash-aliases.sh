#!/bin/bash

alias webapp="cd $WEBAPP_PATH"
alias devenv="cd $DEVENV_PATH"

# alias vgup="cd $DEVENV_PATH && vagrant up && cd -"
# alias vgssh="vagrant ssh `vagrant global-status | grep devenv | awk '{print $1}'`"

# alias ssh="ssh -F `dirname $0`/ssh_config"

merchant() {
	cd $DEVENV_PATH
	bin/php php_webapp src/webapp/scripts/dev_tools/find-merchant.php $1
	cd -
}