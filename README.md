# Configuring #

* `git clone git@bitbucket.org:o10g/terminal-config.git ~/Scripts`
* Copy `.env.dist` to `.env` file
* Edit `.env` file to match your requirements
* Add `source ~/Scripts/init.sh` to your `~/.profile` or `~/.bash_profile`